%global pkg_name %{name}

%global gem_name jwt 
Name: rubygem-%{gem_name}
Version: 2.2.1
Release: 2
Summary: JSON Web Token implementation in Ruby
Group: Development/Languages
License: MIT
URL: https://github.com/jwt/ruby-jwt
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Source1: https://github.com/jwt/%{gem_name}/archive/refs/tags/%{gem_name}-%{version}.tar.gz

# start specfile generated dependencies
Requires: ruby(release)
Requires: ruby >= 2.1
Requires: ruby(rubygems)
BuildRequires: ruby(release)
BuildRequires: ruby >= 2.1
BuildRequires: rubygems-devel
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}
# end specfile generated dependencies

%description
A pure ruby implementation of the RFC 7519 OAuth JSON Web Token (JWT)
standard.

%package doc
Summary: Documentation for %{pkg_name}
Group: Documentation
Requires: %{pkg_name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{pkg_name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -pa .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%files
%dir %{gem_instdir}
%exclude %{gem_instdir}/.codeclimate.yml
%exclude %{gem_instdir}/.ebert.yml
%exclude %{gem_instdir}/.gitignore
%exclude %{gem_instdir}/.rubocop.yml
%exclude %{gem_instdir}/.travis.yml
%license %{gem_instdir}/LICENSE
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/AUTHORS
%doc %{gem_instdir}/Appraisals
%exclude %{gem_instdir}/.rspec
%doc %{gem_instdir}/CHANGELOG.md
%{gem_instdir}/Gemfile
%doc %{gem_instdir}/README.md
%{gem_instdir}/Rakefile
%{gem_instdir}/ruby-jwt.gemspec

%changelog
* Sun Apr 24 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 2.2.1-2
- Modify changelog and add yaml file

* Mon Jun 07 2021 liqiuyu <liqiuyu@kylinos.cn> - 2.2.1-1
- Init project
